var cols = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--columns')); 
var rows = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--rows')); 
const grille = document.querySelector(".grille");
const repere = document.querySelector(".repere");
const lettres = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
const chiffres = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30']

for (let i = 0; i < (cols * rows); i++) {
    let div = document.createElement('div');
    grille.appendChild(div);
}

for (let i = 0; i < cols; i++) {
    let div = document.createElement('div');
    div.classList.add('chiffres');
    div.innerHTML = chiffres[i];
    repere.appendChild(div);
}

for (let i = 0; i < rows; i++) {
    let div = document.createElement('div');
    div.classList.add('lettres');
    div.innerHTML = lettres[i];
    repere.appendChild(div);
}

const button_grille = document.querySelector(".button");

function visible_hidden() {
var hiddenAll = document.querySelectorAll(".hidden");
var visibleAll = document.querySelectorAll(".visible");
var hidden = document.querySelector(".hidden");
var visible = document.querySelector(".visible");

    if(hidden){
        console.log("yo");
        for (let i = 0; i < hiddenAll.length; i++) {
            hiddenAll[i].classList.add("visible");
            hiddenAll[i].classList.remove("hidden");
            button_grille.innerHTML = "Cacher la grille";
        }
    } else if(visible){
        console.log("yehyehyeh");
        for (let a = 0; a < visibleAll.length; a++) {
            visibleAll[a].classList.add("hidden");
            visibleAll[a].classList.remove("visible");
            button_grille.innerHTML = "Afficher la grille";
        }
    }
}

function imprimer_page(){
    window.print();
}